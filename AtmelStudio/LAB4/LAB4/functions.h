#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


void init_TC0();
void init_TC2();
void init_LED();
void init_TC0_ints();
void init_PWM();
void delay();
void delay_ms(unsigned int mode);
void setLED(bool);
void toggleLED();