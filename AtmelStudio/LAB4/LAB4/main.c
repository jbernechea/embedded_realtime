#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "functions.h"

#define F_CPU 16000000

 int cycle_num = 0; // tracks number of 250ms segments
 int of_count = 0; // tracks number of overflows
 int do_a = 1, do_b = 1, do_c = 0; //flags
unsigned int a_time = 250, b_time = 500, c_time = 1000; //computing times


ISR(TIMER0_OVF_vect)
{
	
	TIFR0 |= (1 << TOV0);	//reset overflow flag
	of_count++;				//Increase counter that controls time
							//since last multiple of 250ms
	if (of_count == 250) {

		cycle_num ++;

		// raise flags if needed
		switch (cycle_num)
		{
			case 1:

				do_a = 1;
				do_c = 1;

			break;

			case 2:

				do_a = 1;
				do_b = 1;

			break;

			case 3:

				do_a = 1;

			break;

			case 4:

				do_a = 1;
				do_b = 1;
				cycle_num = 0;

			break;
		}
		of_count = 0;
	}

	TCCR0B |= (1 << CS00); //restart timer

}

void task_A(){

	//definition of task_A
	setLED(1); //turn on the led
	delay_ms(a_time);
	do_a = 0;

}

void task_B(){

	//definition of task_B
	setLED(0); //turn off the led
	do_b = 0;
	delay_ms(b_time);
}

void task_C(){

	//definition of task_C
	setLED(0); //turn off the led
	delay_ms(c_time);
	do_c = 0;

}


// main program
//--------------

int main(void)
{
	init_LED();
	init_TC0_ints();
	init_TC2();
	
	// forever loop
	while(1){
		
		if (do_a) {
			task_A();
		}

		else if (do_b) {
			task_B();
		}

		else if (do_c) {
			task_C();
		}
		
	}

}

