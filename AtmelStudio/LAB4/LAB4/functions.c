#include "functions.h"


void init_TC0()
{
	// initialize the Timer/Counter0 (TC0)
	
	TCCR0A = 0x00;			// Normal mode
	TCCR0B |= (1<<CS02);	// Prescaler = 256
}

void init_LED()
{
	// PB5 (connected to LED) is output
	
	DDRB |= 0b00100000;
}

void init_TC0_ints()
{
	
	TCCR0A = 0x00;			// Normal mode
	TCCR0B |= (3<<CS00);	// Prescaler = 64
	TIMSK0 |= (1<<TOIE0);	// enable Overflow Interrupts
	sei();					// Global Interrupt Enable
}

void init_TC2()
{
	TCCR2A = 0x00;			// Normal mode
	TCCR2B |= (1<<CS22);	// Prescaler = 64
}



void init_PWM()
{	
	DDRB |= 0b00000010;		// Configure pin 9 (OC1A) as an output
	TCCR1A = 0b10000001;	//
	TCCR1B = 0b00001001;	//
	OCR1A = 0;				//Value of PWM to 0
}


void delay()
{
	// delay 1 second function (based on TC0)
	
	unsigned int i;
	for (i=1;i<=244;i++)
	{
		TCNT0 = 0;						// init count
		while((TIFR0 & (1<<TOV0))==0);	// wait overflow
		TIFR0 |= (1<<TOV0);				// clear the overflow flag (by writing a '1'!)
	}
}

void delay_ms(unsigned int mode){
	// generate a x ms delay (based on TC2)
	for (unsigned int i = 1; i <= mode; i++) // mode
	{
		TCNT2 = 0;						// init count
		while((TIFR2 & (1<<TOV2))==0);	// wait overflow
		TIFR2 |= (1<<TOV2);				// clear the overflow flag
	}
}

void setLED(bool state){
	
	if (state)
		PORTB |= 0x20;	// PB5=1 -> LED ON
	else
		PORTB &= 0xDF;	// PB5=0 -> LED OFF
	
}

void toggleLED(){
	
	PORTB ^= (1 << 5);

}