#include "functions.h"


void init_TC0()
{
	// initialize the Timer/Counter0 (TC0)
	
	TCCR0A = 0x00;			// Normal mode
	TCCR0B |= (1<<CS02);	// Prescaler = 256
}

void init_LED()
{
	// PB5 (connected to LED) is output
	
	DDRB = 0b00100000;
}

void init_TC0_ints()
{
	TCCR0A = 0x00;			// Normal mode
	TCCR0B |= (1<<CS02);	// Prescaler = 256
	TIMSK0 |= (1<<TOIE0);	// enable Overflow Interrupts
	SREG |= 0x80;			// Global Interrupt Enable
}


void delay()
{
	// delay 1 second function (based on TC0)
	
	unsigned int i;
	for (i=1;i<=244;i++)
	{
		TCNT0 = 0;						// init count
		while((TIFR0 & (1<<TOV0))==0);	// wait overflow
		TIFR0 |= (1<<TOV0);				// clear the overflow flag (by writing a '1'!)
	}
}

void setLED(bool state){
	
	if (state)
		PORTB |= 0x20;	// PB5=1 -> LED ON
	else
		PORTB &= 0xDF;	// PB5=0 -> LED OFF
	
}

void toggleLED(){
	
	PORTB ^= (1 << 5);

}