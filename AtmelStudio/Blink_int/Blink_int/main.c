#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "functions.h"

#define F_CPU 16000000

ISR(TIMER0_OVF_vect)
{
	static unsigned int i=0;

	i++;
	if (i>=244)
	{
		toggleLED();
		i=0;
	}
}



// main program
//--------------

int main(void)
{


	// initialize Timer/Counter0
	init_TC0_ints();
	init_LED();

	// forever loop
	while(1)
	{
	
		
	}

	return 0;
}
