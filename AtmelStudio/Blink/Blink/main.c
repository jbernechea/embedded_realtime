#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "functions.h"

#define F_CPU 16000000



// main program
//--------------

int main(void)
{


	// initialize Timer/Counter0
	init_TC0();
	init_LED();

	// forever loop
	while(1)
	{

		toggleLED();	
		delay();		
		
	}

	return 0;
}
