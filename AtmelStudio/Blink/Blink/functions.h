#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


void init_TC0();
void init_LED();
void delay();
void setLED(bool);
void toggleLED();