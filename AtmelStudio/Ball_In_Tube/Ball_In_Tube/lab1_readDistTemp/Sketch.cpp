﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */
//LAB 1, Measure distance with HC-SR04 sensor

const int EchoPin = 12;
const int TriggerPin = 13;
const int PWM_Pin = 11;
const int RPM_Pin = 10;
int pwm=0;
int rpm = ;
float N;

float a = 0.030857122609246;
float b = 13.832670514718995;

void setup() {
	Serial.begin(9600);
	pinMode(TriggerPin, OUTPUT);
	pinMode(EchoPin, INPUT);
	pinMode(PWM_Pin, OUTPUT);
	pinMode(RPM_Pin, INPUT_PULLUP);
	
}

int measureDist(int TriggerPin, int EchoPin) {
	long duration, distanceCm;

	//Force the pin to have a low signal to make sure
	//it registers a high for 10 microseconds so that
	//the reading is initiated.
	digitalWrite(TriggerPin, LOW);
	delayMicroseconds(5);
	//Initiate sensor with pulse
	digitalWrite(TriggerPin, HIGH);
	delayMicroseconds(10);
	digitalWrite(TriggerPin, LOW);

	//Measure the time the sound travelled measuring
	//the width of the pulse in pin "Echo"
	duration = pulseIn(EchoPin, HIGH);
	//Conversion from time to distance (in cm). Value
	//obtained from the sensor's data-sheet.
	distanceCm = duration/58;
	return distanceCm;
}

float measureRPM(void){
	
	float t1 = pulseIn(RPM_Pin,HIGH);
	float t2 = pulseIn(RPM_Pin,HIGH);
	float t3 = pulseIn(RPM_Pin,HIGH);
	float t4 = pulseIn(RPM_Pin,HIGH);
	float Ts = t1+t2+t3+t4;
	
	float N = 60/(Ts/1000000); // Fan speed in RPM
	
	if (Ts == 0)
		N = 0;
		
	return N;

}




void loop() {
	
	
	if(rpm<5000)
		rpm += 1;
	else
		rpm = 0;
	
	pwm = a*rpm + b;
	
	analogWrite(PWM_Pin,pwm);
	delay(10);
	int cm = measureDist(TriggerPin, EchoPin);
	
	//PRINT
	Serial.print("Distancia: ");
	Serial.println(cm);
	
	Serial.print("RPM: ");
	Serial.println(rpm);
	
	Serial.print("PWM: ");
	Serial.println(pwm);
}

