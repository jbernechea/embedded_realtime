#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "functions.h"

#define F_CPU 16000000

int PWM = 0;

bool edge = true; // true for rising, false for falling
unsigned int pulse = 0;
int distance = 0;

ISR(INT0_vect)
{
	if (edge){	// Rising edge
		
		edge = false; //Next time we get a falling edge
		TCNT0 = 0;	// init count
		EICRA |= (2<<ISC00); // INT0 as FALLING Edge Int.
	}
	
	else{	//Falling edge
		
		EICRA |= (4<<ISC00); // INT0 as RISING Edge Int.
		pulse = TCNT0;
		TCNT0 = 0; //reset
		edge = true;
		
	}


}


// main program
//--------------

int main(void)
{
	init_TC0();
	init_TC2();
	init_PWM();
	init_IO_pins();
	init_interrupts();
	sei();					// Global Interrupt Enable
	// forever loop
	while(1){
		
		trigger_pulse();
	
		distance = pulse/(256*58);
		
		OCR1A = distance;
		delay_us(10000);
		
	}

}
