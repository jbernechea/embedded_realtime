%% Exercise 2: Read distance ultrasonic sensor
clear all
UNO = arduino('COM3','Uno','Libraries','Ultrasonic');
ultrasonicObj = ultrasonic(UNO,'D13','D12')

while(1)
    distance= readDistance(ultrasonicObj); % In meters
end