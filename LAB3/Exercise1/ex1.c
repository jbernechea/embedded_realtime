/*
 * ex1.c
 *
 *  Created on: Nov 4, 2019
 *      Author: koldovich15
 */

#include <avr/io.h>
#include <avr/interrupt.h>

ISR(TIMER0_OVF_vect)
{
	static unsigned char i=0, pwm = 0;

	i++;
	if (i==244)
	{
		i = 0;
		if(pwm < 255){
			pwm += 15;
			OCR1A = pwm;
		}
		else
			pwm = 0;

	}
}



int main(void)
{

	TCCR0A = 0x00;			// Normal mode
	TCCR0B |= (1<<CS02);	// Prescaler = 256
	TIMSK0 |= (1<<TOIE0);	// enable Overflow Interrupts
	SREG |= 0x80;			// Global Interrupt Enable

	DDRB |= 0b00000010; // Configure pin 9 (OC1A) as an output
	TCCR1A = 0b10000001;
	TCCR1B = 0b00001001;

	OCR1A = 0;

	while(1);


}
