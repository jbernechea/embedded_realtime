/*
 * ex1.c
 *
 *  Created on: Nov 4, 2019
 *      Author: koldovich15
 */

#include <avr/io.h>
#include <avr/interrupt.h>

ISR(INT0_vect)
{
	PORTB = 0x20;
}



int main(void)
{

	TCCR0A = 0x00;			// Normal mode
	TCCR0B |= (1<<CS02);	// Prescaler = 256
	TIMSK0 |= (1<<TOIE0);	// enable Overflow Interrupts
	SREG |= 0x80;			// Global Interrupt Enable

	// Configure external interruption
	EICRA = 0b00000011; // Configure INT0 (rising edge in INT0 creates an interrupt request.
	EIMSK = 0b00000001; // External INT0 request enabled

	DDRB = 0b00100010; // Configure pin 9 (OC1A) as an output
	DDRD = 0b00000000;
	TCCR1A = 0b10000001;
	TCCR1B = 0b00001001;

	OCR1A = 0;
	PORTB = 0x00;
	PORTD = 0b00100000;

	while(1);


}





