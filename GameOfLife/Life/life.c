#include <ncurses.h>
#include <stdlib.h>
#include <menu.h>
#include "life.h"

#define TIME_OUT  300

int STARTX = 0; //Initial X position of screen
int STARTY = 0; //Initial Y position of screen
int ENDX = 0; //Final X position of screen
int ENDY = 0; //Final Y position of screen

struct state;

int main()
{

	// Coder 1
	//Initialization
	initscr();
	cbreak();
	timeout(TIME_OUT);
	keypad(stdscr, TRUE);
	int i, j;

	ENDX = COLS - 1;
	ENDY = LINES - 1;

	// Coder 2
	//Memory management: allocating
	state **workarea = (state **)malloc(COLS*sizeof(state *));
	for(i = 0;i < COLS; ++i)
		workarea[i] = (state *)malloc(LINES*sizeof(state));

	// Coder 1
	init_figure(workarea); //Display menu to choose the initial workarea
	update_state(workarea, STARTX, STARTY, ENDX, ENDY); //Update old states

	// Coder 3
	//GAME OF LIFE STARTS
	while(getch() != 32)
	{
		for(i = STARTX; i <= ENDX; ++i)
			for(j = STARTY; j <= ENDY; ++j)
				neighbours(workarea, i, j, COLS, LINES); //Get neighbours and GoL rules
		update_state(workarea, STARTX, STARTY, ENDX, ENDY);//Update old states
		display(stdscr,  workarea, STARTX, STARTY, ENDX, ENDY);//Display new states
	}
	// GAME OVER

	// Coder 2
	// Memory management: deallocating
	for(i = 0;i < COLS; ++i)
		free(workarea[i]);
	free(workarea);
	endwin();
	return 0;
}
