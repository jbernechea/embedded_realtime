
// Our state structure

typedef struct _state
{
	int oldstate;
	int newstate;
}state;

//display function
void display(WINDOW *win, state **area, int startx, int starty, int endx, int endy);
//neighbours function
void neighbours(state **area, int x, int y, int cols, int lines);
//update_state function
void update_state(state **area, int startx, int starty, int endx, int endy);
//initial blocks definition
void init_figure(state **area);
//menu items display
int menu_display();
