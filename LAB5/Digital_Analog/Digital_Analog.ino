// FreeRTOS example: programming 2 periodic tasks
//------------------------------------------------

#include <Arduino_FreeRTOS.h>
#include <semphr.h>

// define two tasks for Blink & AnalogRead
void ReadDigital( void *pvParameters );
void ReadAnalog( void *pvParameters );

SemaphoreHandle_t xSerialSemaphore;

//-------
// setup
//-------

void setup() {

  Serial.begin(9600);

    if ( xSerialSemaphore == NULL )  // Check to confirm that the Serial Semaphore has not already been created.
  {
    xSerialSemaphore = xSemaphoreCreateMutex();  // Create a mutex semaphore we will use to manage the Serial Port
    if ( ( xSerialSemaphore ) != NULL )
      xSemaphoreGive( ( xSerialSemaphore ) );  // Make the Serial Port available for use, by "Giving" the Semaphore.
  }

  // create first task
  xTaskCreate(
    ReadDigital
    ,  (const portCHAR *)"Digital"  // name
    ,  128                        // stack size
    ,  NULL
    ,  1                          // higher priority
    ,  NULL );

  // create second task
  xTaskCreate(
    ReadAnalog
    ,  (const portCHAR *) "Analog"
    ,  128 
    ,  NULL
    ,  1                          // lower priority
    ,  NULL );

  // now the task scheduler is automatically started
}


//------
// loop
//------

void loop()
{
  // empty!
}


//--------------------------
// first task: LED blinking
//--------------------------

void ReadDigital(void *pvParameters)
{
  (void) pvParameters;

  // digital pin 13 (connected to built-in LED) as output 

   pinMode(11, INPUT);
   bool digital = false;
   
  // forever loop
  for (;;) 
  {
    digital = digitalRead(11);
        if ( xSemaphoreTake( xSerialSemaphore, ( TickType_t ) 5 ) == pdTRUE )
    {

       Serial.print("Reading Digital Input: ");
       Serial.println(digital); 

       xSemaphoreGive( xSerialSemaphore ); // Now free or "Give" the Serial Port for others.
    }
                
    vTaskDelay(1);  
    
  }
}


//------------------------
// first task: reading A0
//------------------------

void ReadAnalog(void *pvParameters)
{
  (void) pvParameters;

  // initialize serial communication at 9600 bits per second
  Serial.begin(9600);

  // forever loop
  for (;;)
  {
    int sensorValue = analogRead(A0);
    if ( xSemaphoreTake( xSerialSemaphore, ( TickType_t ) 5 ) == pdTRUE )
    {

      Serial.print("Reading Analog Input: ");// read analog input A0
      Serial.println(sensorValue);       // send through serial port

      xSemaphoreGive( xSerialSemaphore ); // Now free or "Give" the Serial Port for others.
    }

    vTaskDelay(1);                     // one tick (smallest) delay
  }
} 
