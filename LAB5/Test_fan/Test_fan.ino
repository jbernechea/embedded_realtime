// FreeRTOS example: programming 2 periodic tasks
//------------------------------------------------

#include <Arduino_FreeRTOS.h>
#include <semphr.h>

// define two tasks for Blink & AnalogRead
void ChangePWMValue( void *pvParameters );
void ReadSpeedRPM( void *pvParameters );

SemaphoreHandle_t xSerialSemaphore;

float pwm;
float sp = 2000;
float N;
//-------
// setup
//-------

void setup() {

  Serial.begin(9600);
  

    if ( xSerialSemaphore == NULL )  // Check to confirm that the Serial Semaphore has not already been created.
  {
    xSerialSemaphore = xSemaphoreCreateMutex();  // Create a mutex semaphore we will use to manage the Serial Port
    if ( ( xSerialSemaphore ) != NULL )
      xSemaphoreGive( ( xSerialSemaphore ) );  // Make the Serial Port available for use, by "Giving" the Semaphore.
  }

  // create first task
  xTaskCreate(
    ChangePWMValue
    ,  (const portCHAR *)"PWM"    // name
    ,  128                        // stack size
    ,  NULL
    ,  1                          
    ,  NULL );

  // create second task
  xTaskCreate(
    ReadSpeedRPM
    ,  (const portCHAR *) "Speed"
    ,  128 
    ,  NULL
    ,  1                          
    ,  NULL );

  // now the task scheduler is automatically started
}


//------
// loop
//------

void loop()
{
  // empty!
}


//--------------------------
// first task: LED blinking
//--------------------------

void ChangePWMValue(void *pvParameters)
{
  (void) pvParameters;
  
  const int PWM_Pin = 11;
  pinMode(PWM_Pin, OUTPUT);
 
  // forever loop
  for (;;) 
  {
    // Ecuation values
    float a = -6.42416285724478e-07;
    float b = 0.0381259941454510;
    float c = 4.54155397663632;

 
    if(sp == 2000)
      sp = 3000;
    else
      sp = 2000;

    pwm = a*sp*sp + b*sp + c;
    analogWrite(PWM_Pin,round(pwm));
 
    if ( xSemaphoreTake( xSerialSemaphore, ( TickType_t ) 5 ) == pdTRUE )
    {

       xSemaphoreGive( xSerialSemaphore ); // Now free or "Give" the Serial Port for others.
    }
                
    vTaskDelay(10000 / portTICK_PERIOD_MS);  
    
  }
}


//------------------------
// first task: reading A0
//------------------------

void ReadSpeedRPM(void *pvParameters)
{
  (void) pvParameters;
  const int RPM = 10;
  // initialize serial communication at 9600 bits per second
  Serial.begin(9600);
  pinMode(RPM, INPUT_PULLUP);


  // forever loop
  for (;;)
  {
    float t1 = pulseIn(RPM,HIGH);
    float t2 = pulseIn(RPM,HIGH);
    float t3 = pulseIn(RPM,HIGH);
    float t4 = pulseIn(RPM,HIGH);
    float Ts = t1+t2+t3+t4;
    float N = 60/(Ts/1000000); // Fan speed in RPM
    if (Ts == 0)
      N = 0;
    if ( xSemaphoreTake( xSerialSemaphore, ( TickType_t ) 5 ) == pdTRUE )
    {

      Serial.print(sp);
      Serial.print(",");
      Serial.println(N);       // send through serial port

      xSemaphoreGive( xSerialSemaphore ); // Now free or "Give" the Serial Port for others.
    }

    vTaskDelay(50 / portTICK_PERIOD_MS);  
  }
} 
