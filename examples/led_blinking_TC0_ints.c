/*
 * led_blinking_TC0_ints.c
 *
 *  Created on: 07/10/2016
 *      Author: stornil
 */


#include <avr/io.h>
#include <avr/interrupt.h>


// function to initialize TC0 (Timer/Counter0) and Interrupts
//-----------------------------------------------------------

void init_TC0_ints()
{
	TCCR0A = 0x00;			// Normal mode
	TCCR0B |= (1<<CS02);	// Prescaler = 256
	TIMSK0 |= (1<<TOIE0);	// enable Overflow Interrupts
	SREG |= 0x80;			// Global Interrupt Enable
}

// Interrupt Service Routine for TC0 Overflow
//-------------------------------------------

ISR(TIMER0_OVF_vect)
{
	static unsigned char i=0,j=0;

	i++;
	if (i==100)
	{
		i = 0;

		// the following code is executed one time each 100 interrupts

		j = (j+1)%2;	// j=0,1,0,1,0,...

		if (j==0)
			PORTB |= 0x20;	// LED ON
		else
			PORTB &= 0xDF;	// LED OFF

	}
}

// main program
//--------------

int main(void)
{

	// PB5 (connected to LED) is output
	DDRB = 0b00100000;

	// initialize Timer/Counter0 and associated interrupts
	init_TC0_ints();

	// forever loop
	while(1)
	{
		// empty loop!
	}

	return 0;
}





