//LAB 1, Measure distance and temperature

const int EchoPin = 12;
const int TriggerPin = 13;
const int TemperaturePin= A0;
const int SourcePin= 2;

void setup() {
   Serial.begin(9600);
   pinMode(TriggerPin, OUTPUT);
   pinMode(EchoPin, INPUT);
   pinMode(SourcePin, OUTPUT);
   digitalWrite(SourcePin, HIGH);
}

void loop() {

   float cm = measureDist(TriggerPin, EchoPin);

   Serial.print("Distancia: ");
   Serial.print(cm);
   Serial.println("");

   delay(1000);
}

float measureDist(int TriggerPin, int EchoPin) {
   float duration, distanceCm;
   float temp;

   //Force the pin to have a low signal to make sure
   //it registers a high for 10 microseconds so that
   //the reading is initiated.
   digitalWrite(TriggerPin, LOW);
   delayMicroseconds(5);
   digitalWrite(TriggerPin, HIGH);  //Initiate sensor
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);

   //Measure the time the sound travelled measuring
   //the width of the pulse in pin "Echo"
   duration = pulseIn(EchoPin, HIGH); //Read time

   temp = measureTemp(TemperaturePin);

   //Apply formula of gases speed in air (cm/s) that will vary according
   //to the measured temperature.
   float soundvel = sqrt(1.4*8.314*(temp+273.0)/0.029)*100.0/1000000.0;

   //Conversion of travel time of the sound into distance
   distanceCm = soundvel*duration/2.0;
   return distanceCm;
}

float measureTemp(int sensorPin) {

  int value = analogRead(sensorPin);
  //Convert the code obtained from the ADC
  //conversion into a milivolt measurement.
  float millivolts = (value/1023.0)*5000;
  //Conversion from volts to celsius degrees
  //according to data-sheet.
  float celsius = millivolts/10;
  return celsius;

}
