//LAB 1, Measure TEMPERATURE with LM-35 sensor

const int sensorPin= A0;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  int value = analogRead(sensorPin);
  //Convert the code obtained from the ADC
  //conversion into a milivolt measurement.
  float millivolts = (value/1023.0)*5000;
  //Conversion from volts to celsius degrees
  //according to data-sheet.
  float celsius = millivolts/10;
  Serial.print(celsius);
  Serial.println(" C");
  delay(1000);
}
