<<<<<<< HEAD
//LAB 1, Give different speed values to the fan and read its speed

const int PWM_Pin= 11;
const int RPM = 10;

void setup()
{
  Serial.begin(9600);
  pinMode(PWM_Pin, OUTPUT);
  //The the input a pull up resistor is added in order to measure properly the speed of the fan.
  //It is obtained through the datasheet of the fan.
  pinMode(RPM, INPUT_PULLUP);
}

void loop()
{
  int pwm;
  float N;

  //Initialize pwm signal to 0 in order to have a clean reading
  pwm = 0;
  analogWrite(PWM_Pin,pwm);
  delay(5000);
  //Every step the PWM signal given to the fan increases by one and the speed of the fan is measured
  for(pwm=0; pwm<256; pwm++){
    analogWrite(PWM_Pin,pwm);
    delay(100);
    N = measureSpeed(RPM);
    Serial.print(pwm);
    Serial.print(",");
    Serial.println(N);
  }

}

float measureSpeed(int sensorPIN)
{

  //Four high pulses are measured and it
  //returns RPM.
  float t1 = pulseIn(sensorPIN,HIGH);
  float t2 = pulseIn(sensorPIN,HIGH);
  float t3 = pulseIn(sensorPIN,HIGH);
  float t4 = pulseIn(sensorPIN,HIGH);
  float Ts = t1+t2+t3+t4;
  float N = 60/(Ts/1000000); // Fan speed in RPM

  return N;
}
=======
//LAB 1, Give different speed values to the fan and read its speed

const int PWM_Pin= 11;
const int RPM = 10;

void setup()
{
  Serial.begin(9600);
  pinMode(PWM_Pin, OUTPUT);
  pinMode(RPM, INPUT_PULLUP);
}

void loop()
{
  int pwm;
  float N;

  /*
  Bucle donde aumenta el valor de PWM en 1,
  espera 0.1 segundo para dar tiempo al ventilador
  a reaccionar y mide la velocidad UNA VEZ.
  Igual interesa cambiar el valor y hacer unas
  cuantas mediciones??
  */
   analogWrite(PWM_Pin,0);
   delay(5000);
  for(pwm=0; pwm<256+50; pwm++){
    analogWrite(PWM_Pin,pwm);
    delay(100);
    N = measureSpeed(RPM);
    Serial.print(pwm);
    Serial.print(",");
    Serial.println(N);
  }

}

float measureSpeed(int sensorPIN)
{

  //Four high pulses are measured and it
  //returns RPM.
  float t1 = pulseIn(sensorPIN,HIGH);
  float t2 = pulseIn(sensorPIN,HIGH);
  float t3 = pulseIn(sensorPIN,HIGH);
  float t4 = pulseIn(sensorPIN,HIGH);
  float Ts = t1+t2+t3+t4;
  float N = 60/(Ts/1000000); // Fan speed in RPM

  return N;
}
>>>>>>> master
