figure;
plot(ONOFFcontroldata(:,1));
hold on
plot(ONOFFcontroldata(:,2));
legend('Position of the ball','Desired position');
title('Position of the ball when reference is 15cm');
xlabel('Time instant');
ylabel('Heigth (cm)');

%% ref=5

figure;
plot(ONOFFcd5(:,1));
hold on
plot(ONOFFcd5(:,2));
legend('Position of the ball','Desired position');
title('Position of the ball when reference=5cm');
xlabel('Time instant');
ylabel('Heigth (cm)');
axis([0 1807 0 20])